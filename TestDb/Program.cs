﻿using System;
using System.Linq;

namespace TestDb
{
    class Program
    {
        static void Main(string[] args)
        {
            //var person = new Person
            //{
            //    Name = "Amin",

            //    Phone = "09120003241"
            //};

            // Insert

            //using (var ctx = new AppDbContext())
            //{
            //    //ctx.Persons.Add(person);

            //    ctx.SaveChanges();
            //}

            // Select

            //var ctx = new AppDbContext();

            //var persons = ctx.Persons.Where(c=>c.Name == "Amin");

            //foreach (var person in persons)
            //{
            //   Console.WriteLine($"{person.Name}");
            //}

            // Update

            using (var ctx = new AppDbContext())
            {
                var person = ctx.Persons.Where(c => c.Name == "Amin").FirstOrDefault();

                person.Phone = "09332145376";

                ctx.SaveChanges();
            }

            Console.ReadKey();
        }
    }
}
