﻿using Microsoft.EntityFrameworkCore;

namespace TestDb
{
    public class AppDbContext : DbContext
    {
        public DbSet<Person> Persons { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=.;Database=PeopleDb;Trusted_Connection=True;");

            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Person>().ToTable("Person");
            modelBuilder.Entity<Person>().Property(p=>p.Name).HasMaxLength(20);
            modelBuilder.Entity<Person>().Property(p=>p.Phone).HasMaxLength(11);

            base.OnModelCreating(modelBuilder);
        }
    }
}
